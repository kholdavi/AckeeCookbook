package me.khol.ackeecookbook

import android.content.Context
import me.khol.ackeecookbook.di.ServiceModule
import me.khol.ackeecookbook.domain.response.ErrorResponse
import retrofit2.HttpException
import retrofit2.Response
import android.net.ConnectivityManager


/**
 * Utils
 *
 * Created by DAVE on 19. 7. 2017.
 */
object Utils {

    fun stripTime(time: CharSequence): Int {
        return time.toString().replace(Regex("[^0-9]"), "").toInt()
    }

    fun parseErrorResponse(e: HttpException): ErrorResponse {
        return parseErrorBody(e.response(), ErrorResponse::class.java)
    }

    fun <T> parseErrorBody(response: Response<*>, clazz: Class<T>): T {
        return ServiceModule.GSON.fromJson(response.errorBody()?.charStream(), clazz)
    }

    fun isConnected(): Boolean {
        val cm = App.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        val isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting
        return isConnected
    }

}
