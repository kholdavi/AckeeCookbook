package me.khol.ackeecookbook

import android.app.Application
import me.khol.ackeecookbook.R
import me.khol.ackeecookbook.di.AppComponent
import me.khol.ackeecookbook.di.AppModule
import me.khol.ackeecookbook.di.DaggerAppComponent
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

/**
 * Application class on steroids
 * Created by DAVE on 18. 7. 2017.
 */
class App : Application() {


    companion object {
        private lateinit var sInstance: App

        fun getInstance(): App {
            return sInstance
        }

        fun getAppComponent(): AppComponent {
            return getInstance().appComponent
        }
    }

    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        sInstance = this

        appComponent = DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()

        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.roboto_regular))
                .setFontAttrId(R.attr.fontPath)
                .build()
        )
    }
}