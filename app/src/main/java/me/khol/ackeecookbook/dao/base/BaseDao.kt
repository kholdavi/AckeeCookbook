package me.khol.ackeecookbook.dao.base

import com.hannesdorfmann.sqlbrite.dao.Dao
import io.reactivex.Observable


/**
 * Base DAO that all DAOs should extend from. Add extra convenience methods
 *
 * Created by DAVE on 18. 7. 2017.
 */
abstract class BaseDao<T> : Dao() {

    fun insertInBatch(data: List<T>): Observable<Boolean> {
        return Observable.create({ sub ->
            val t = newTransaction()
            try {
                for (d in data) {
                    insertItem(d)
                }
                t.markSuccessful()
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                t.end()
            }
            sub.onNext(true)
            sub.onComplete()
        })
    }

    fun updateData(data: List<T>): Observable<Boolean> {
        return Observable.create({ sub ->
            val t = newTransaction()
            try {
                clearTable()

                for (d in data) {
                    insertItem(d)
                }
                t.markSuccessful()
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                t.end()
            }
            sub.onNext(true)
            sub.onComplete()
        })
    }

    abstract fun insertItem(d: T): Long

    fun clearTable(): Long {
        return db.delete(getTableName(), null).toLong()
    }

    protected abstract fun getTableName(): String


}