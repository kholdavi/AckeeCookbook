package me.khol.ackeecookbook.dao

import android.database.sqlite.SQLiteDatabase
import me.khol.ackeecookbook.dao.base.BaseDao
import me.khol.ackeecookbook.domain.model.Recipe

/**
 * DAO for recipes
 *
 * Created by DAVE on 18. 7. 2017.
 */
class RecipeDao : BaseDao<Recipe>() {

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented")
    }

    override fun insertItem(d: Recipe): Long {
        TODO("not implemented")
    }

    override fun getTableName(): String {
        TODO("not implemented")
    }

    override fun createTable(database: SQLiteDatabase?) {
        TODO("not implemented")
    }

}