package me.khol.ackeecookbook

/**
 * API configuration
 *
 * Created by DAVE on 18. 7. 2017.
 */

object ApiConfig {

    val BASE_URL = "https://cookbook.ack.ee/"

}