package me.khol.ackeecookbook.interactor

import io.reactivex.Completable
import io.reactivex.Single
import me.khol.ackeecookbook.domain.model.Recipe
import me.khol.ackeecookbook.domain.ApiDescription
import me.khol.ackeecookbook.domain.model.RecipeDetail
import me.khol.ackeecookbook.domain.model.Score
import me.khol.ackeecookbook.domain.model.UpdateBody
import me.khol.ackeecookbook.domain.response.ScoreResponse


/**
 * Implementation of interface for communicating with API service
 *
 * Created by DAVE on 18. 7. 2017.
 */
class ApiInteractorImpl(val apiDescription: ApiDescription) : IApiInteractor {

    override fun createRecipe(name: String,
                              description: String,
                              duration: Int,
                              ingredients: List<String>,
                              info: String): Single<RecipeDetail> {
        val recipe = RecipeDetail(name, description, duration, ingredients, info, null, null)
        return createRecipe(recipe)
    }

    override fun createRecipe(recipe: RecipeDetail): Single<RecipeDetail> {
        return apiDescription.createRecipe(recipe)
    }

    override fun getRecipe(recipeId: String): Single<RecipeDetail> {
        return apiDescription.getRecipe(recipeId)
    }

    override fun updateRecipe(recipeId: String, body: RecipeDetail): Completable {
        return apiDescription.updateRecipe(recipeId, body)
    }

    override fun deleteRecipe(recipeId: String): Completable {
        return apiDescription.deleteRecipe(recipeId)
    }

    override fun addRating(recipeId: String, rating: Int): Single<ScoreResponse> {
        return apiDescription.addRating(recipeId, Score(rating))
    }

    override fun getRecipes(): Single<List<Recipe>> {
        return apiDescription.getRecipes()
    }

}