package me.khol.ackeecookbook.interactor

import io.reactivex.Completable
import io.reactivex.Single
import me.khol.ackeecookbook.domain.model.Recipe
import me.khol.ackeecookbook.domain.model.RecipeDetail
import me.khol.ackeecookbook.domain.response.ScoreResponse

/**
 * Interface for communicating with API service
 *
 * Created by DAVE on 18. 7. 2017.
 */
interface IApiInteractor {

    fun getRecipes(): Single<List<Recipe>>

    fun createRecipe(name: String,
                     description: String,
                     duration: Int,
                     ingredients: List<String>,
                     info: String): Single<RecipeDetail>

    fun createRecipe(recipe: RecipeDetail): Single<RecipeDetail>

    fun getRecipe(recipeId: String): Single<RecipeDetail>

    fun updateRecipe(recipeId: String, body: RecipeDetail): Completable

    fun deleteRecipe(recipeId: String): Completable

    fun addRating(recipeId: String, rating: Int): Single<ScoreResponse>

}