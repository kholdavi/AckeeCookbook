package me.khol.ackeecookbook.presenter

import android.os.Bundle
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.khol.ackeecookbook.App
import me.khol.ackeecookbook.domain.model.Recipe
import me.khol.ackeecookbook.presenter.base.BasePresenter
import me.khol.ackeecookbook.view.RecipeListView
import javax.inject.Inject
import me.khol.ackeecookbook.interactor.IApiInteractor

/**
 * Created by DAVE on 17. 7. 2017.
 *
 * Presenter for RecipeList screen
 */
open class RecipeListPresenter : BasePresenter<RecipeListView>() {

    val TAG = javaClass.name

    @Inject
    lateinit var apiInteractor: IApiInteractor

    var isRefreshing: Boolean = false
    var isLoading: Boolean = false
    val isLastPage: Boolean
        get() {
            return recipes.size > 10
        }

    var recipes: MutableList<Recipe> = mutableListOf()

    override fun onCreate(savedState: Bundle?) {
        super.onCreate(savedState)
        App.getAppComponent().inject(this)
    }

    override fun onTakeView(view: RecipeListView) {
        super.onTakeView(view)

        view.showRecipes(recipes, !isLastPage)
    }

    fun refreshRecipes() {
        firstView().subscribe { v ->
            isRefreshing = true
            v.showRefreshing(isRefreshing)
        }

        apiInteractor.getRecipes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .deliverWithView()
                .subscribeSplit({ v, newRecipes ->
                    isRefreshing = false
                    v.showRefreshing(isRefreshing)
                    recipes = newRecipes.toMutableList()
                    v.showRecipes(recipes.toList(), !isLastPage)
                }, { v, t ->
                    t.printStackTrace()
                })
                .disposeWithPresenter()

    }

    fun downloadMoreRecipes(): Boolean {
        if (isLoading || isLastPage)
            return false

        isLoading = true
        apiInteractor.getRecipes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .deliverWithView()
                .subscribeSplit({ v, newRecipes ->
                    isLoading = false
                    recipes.addAll(newRecipes)
                    v.addRecipes(newRecipes.toList(), !isLastPage)
                }, { v, t ->
                    t.printStackTrace()
                })
                .disposeWithPresenter()

        return true
    }

}