package me.khol.ackeecookbook.presenter

import android.os.Bundle
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.khol.ackeecookbook.App
import me.khol.ackeecookbook.R
import me.khol.ackeecookbook.domain.model.RecipeDetail
import me.khol.ackeecookbook.domain.response.ScoreResponse
import me.khol.ackeecookbook.interactor.IApiInteractor
import me.khol.ackeecookbook.presenter.base.BasePresenter
import me.khol.ackeecookbook.ui.activity.RecipeDetailActivity
import me.khol.ackeecookbook.view.RecipeDetailView
import javax.inject.Inject

/**
 * Created by DAVE on 17. 7. 2017.
 *
 * Presenter for RecipeDetail screen
 */
open class RecipeDetailPresenter : BasePresenter<RecipeDetailView>() {

    val TAG = javaClass.name

    @Inject
    lateinit var context: App

    @Inject
    lateinit var apiInteractor: IApiInteractor

    lateinit var recipeId: String


    override fun onCreate(savedState: Bundle?) {
        super.onCreate(savedState)
        App.getAppComponent().inject(this)

        if (savedState != null) {
            init(savedState)
        }
    }

    override fun onTakeView(view: RecipeDetailView) {
        super.onTakeView(view)

        // Rating must be higher than 0??
        fun clampRating(rating: Float): Float {
            if (rating > 5)
                return 5f
            else if (rating <= 0)
                return 1f
            else
                return rating
        }

        view.getRatingChanges()
                .skipInitialValue()
                .doOnNext { view.showRateRecipeLoading(true) }
                .flatMapSingle { rating: Float ->
                    apiInteractor
                            .addRating(recipeId, clampRating(rating).toInt())
                            .subscribeOn(Schedulers.io())
                }
                .observeOn(AndroidSchedulers.mainThread())
                .deliverWithView()
                .subscribeSplit({v, response ->
                    v.showRateRecipeLoading(false)
                    v.disableRatingBar()
                }, { v, t ->
                    v.showRateRecipeLoading(false)
                    v.showSnack(context.getString(R.string.undefined_error))
                    t.printStackTrace()
                })
    }

    fun init(savedState: Bundle) {
        recipeId = savedState.getString(RecipeDetailActivity.ARG_RECIPE_ID)

        firstView().subscribe { v ->
            v.showDetailsLoading(true)
        }

        apiInteractor.getRecipe(recipeId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .deliverWithView()
                .subscribeSplit({ v, recipe: RecipeDetail ->
                    v.showDetailsLoading(false)
                    v.setRecipe(recipe)
                }, { v, t ->
                    v.showDetailsNotAvailable()
                    v.showSnack(context.getString(R.string.undefined_error))
                    t.printStackTrace()
                })
                .disposeWithPresenter()
    }

    override fun onSave(state: Bundle) {
        super.onSave(state)
        state.putString(RecipeDetailActivity.ARG_RECIPE_ID, recipeId)
    }

}