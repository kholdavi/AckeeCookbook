package me.khol.ackeecookbook.presenter

import android.os.Bundle
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Function4
import io.reactivex.schedulers.Schedulers
import me.khol.ackeecookbook.App
import me.khol.ackeecookbook.R
import me.khol.ackeecookbook.Utils
import me.khol.ackeecookbook.domain.model.RecipeDetail
import me.khol.ackeecookbook.interactor.IApiInteractor
import me.khol.ackeecookbook.presenter.base.BasePresenter
import me.khol.ackeecookbook.view.RecipeNewView
import retrofit2.HttpException
import java.net.UnknownHostException
import java.util.ArrayList
import javax.inject.Inject

/**
 * Created by DAVE on 17. 7. 2017.
 *
 * Presenter for RecipeNew screen
 */
open class RecipeNewPresenter : BasePresenter<RecipeNewView>() {

    val TAG = javaClass.name
    val KEY_INGREDIENTS = "ingredients"

    @Inject
    lateinit var context: App

    @Inject
    lateinit var apiInteractor: IApiInteractor

    var ingredients: MutableList<String> = mutableListOf("")


    override fun onCreate(savedState: Bundle?) {
        super.onCreate(savedState)
        App.getAppComponent().inject(this)

        if (savedState != null) {
            ingredients = savedState.getStringArrayList(KEY_INGREDIENTS)
        }
    }

    override fun onSave(state: Bundle) {
        super.onSave(state)
        state.putStringArrayList(KEY_INGREDIENTS, ingredients as ArrayList<String>)
    }

    fun init(view: RecipeNewView) {

        fun is_name_valid(text: CharSequence): Boolean {
            val keyword = context.getString(R.string.recipe_new_recipe_name_keyword)
            return text.contains(keyword, ignoreCase = true)
        }

        fun is_text_valid(text: CharSequence): Boolean {
            return text.isNotBlank()
        }

        fun is_duration_valid(duration: Int): Boolean {
            return duration != 0
        }

        fun <T> show_error_if_empty(changes: Observable<T>,
                                   predicate: (T) -> Boolean,
                                   showErrorFunc: (String?) -> Unit,
                                   message: String): Observable<T> =
                changes .observeOn(AndroidSchedulers.mainThread())
                        .doOnNext { text: T ->
                            showErrorFunc(if (predicate(text)) null else message)
                        }

        val nameChanges = show_error_if_empty(
                view.getNameChanges(),
                ::is_name_valid,
                view::showNameError,
                context.getString(R.string.recipe_new_error_name_must_contain_keyword, context.getString(R.string.recipe_new_recipe_name_keyword))
        )

        val introductionChanges = show_error_if_empty(
                view.getIntroductionChanges(),
                ::is_text_valid,
                view::showIntroductionError,
                context.getString(R.string.recipe_new_error_introduction_cannot_be_empty)
        )

        val processChanges = show_error_if_empty(
                view.getProcessChanges(),
                ::is_text_valid,
                view::showProcessError,
                context.getString(R.string.recipe_new_error_process_cannot_be_empty)
        )

        val durationChanges = show_error_if_empty(
                view.getDurationChanges(),
                ::is_duration_valid,
                view::showDurationError,
                context.getString(R.string.recipe_new_error_time_cannot_be_empty)
        )

        val saveItemMenu = view.getSaveMenuItem()

        Observable.combineLatest(
                nameChanges,
                introductionChanges,
                processChanges,
                durationChanges,
                Function4 {
                    name: CharSequence,
                    introduction: CharSequence,
                    process: CharSequence,
                    duration: Int ->

                    is_name_valid(name) &&
                    is_text_valid(introduction) &&
                    is_text_valid(process) &&
                    is_duration_valid(duration)
                })
                .subscribe { isValid: Boolean ->
                    saveItemMenu.accept(isValid)
                }
                .disposeWithView()

        val allChanges = Observable.combineLatest(
                nameChanges,
                introductionChanges,
                processChanges,
                durationChanges,
                Function4 {
                    name: CharSequence,
                    introduction: CharSequence,
                    process: CharSequence,
                    duration: Int ->

                    RecipeDetail(name.toString(), introduction.toString(), duration, view.getIngredients(), process.toString(), null, null)
                })

        view.getSaveClicks()
                .flatMapSingle { allChanges.firstOrError() }
                .flatMapSingle { recipe ->
                    firstView().subscribe { v ->
                        v.onSaveStarted()
                    }
                    apiInteractor
                            .createRecipe(recipe)
                            .subscribeOn(Schedulers.io())
                }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError { e: Throwable ->
                    when (e) {
                        is HttpException -> {
                            firstView().subscribe { v ->
                                val error = Utils.parseErrorResponse(e)
                                v.onSaveFinishedError(error.message)
                            }
                        }
                        is UnknownHostException -> {
                            firstView().subscribe { v ->
                                v.onSaveFinishedError(context.getString(R.string.undefined_error))
                            }
                        }
                    }
                }
                .retry { e: Throwable ->
                    // Retry in case user has submitted an invalid recipe (because something was
                    // missing or simply because another recipe with the same name already exists)
                    // or because of network error
                    (e is HttpException || e is UnknownHostException)
                }
                .deliverWithView()
                .subscribeSplit({ v, recipe: RecipeDetail ->
                    v.onSaveFinishedSuccess()
                }, { v, e ->
                    v.onSaveFinishedError(context.getString(R.string.undefined_error))
                    e.printStackTrace()
                })
                .disposeWithView()


        view.getNewIngredientClicks()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { _: Any ->
                    ingredients.add("")
                    view.showIngredients(ingredients)
                }
                .disposeWithView()

        view.showIngredients(ingredients)
    }

}