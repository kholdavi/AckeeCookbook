package me.khol.ackeecookbook.presenter.base


import android.os.Bundle
import io.reactivex.*
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import me.khol.ackeecookbook.view.base.BaseView
import nucleus5.presenter.RxPresenter
import nucleus5.presenter.delivery.DeliverLatestCache
import nucleus5.presenter.delivery.Delivery
import nucleus5.view.OptionalView


/**
 * Created by DAVE on 17. 7. 2017.
 *
 * Base class that should all Presenters extend from. Adds useful methods and makes usage of
 * presenters more simple.
 */
abstract class BasePresenter<View : BaseView> : RxPresenter<View>() {


    protected val viewDisposables: MutableList<Disposable> = mutableListOf()
    protected val presenterDisposables: MutableList<Disposable> = mutableListOf()

    protected fun Disposable.disposeWithView(): Disposable {
        viewDisposables.add(this)
        return this
    }

    protected fun Disposable.disposeWithPresenter(): Disposable {
        presenterDisposables.add(this)
        return this
    }

    override fun onDropView() {
        super.onDropView()

        viewDisposables.forEach(Disposable::dispose)
        viewDisposables.clear()
    }

    override fun onDestroy() {
        super.onDestroy()

        presenterDisposables.forEach(Disposable::dispose)
        presenterDisposables.clear()
    }

    open fun onFragmentAttached(fragmentArguments: Bundle) {

    }


    fun <T> Observable<T>.deliverWithView(): Observable<Delivery<View, T>> {
        return compose(deliverLatestCache())
    }

    fun <T> Single<T>.deliverWithView(): Observable<Delivery<View, T>> {
        return toObservable()
                .compose(deliverLatestCache())
    }

    fun <T> Observable<Delivery<View, T>>.subscribeSplit(onNext: (View, T) -> Unit, onError: (View, Throwable) -> Unit): Disposable {
        return subscribe { delivery ->
            delivery.split(onNext, onError)
        }
    }

    /**
     * Returns an [Observable] that emits currently connected [View] or the next view
     * that connects to this presenter if none available.
     */
    fun firstView(): Single<View> {
        return views().firstOrError()
    }

    fun views(): Observable<View> {
        return view()
                .filter { v ->
                    v.view != null
                }.map { v ->
                    v.view!!
                }
    }

}

