package me.khol.ackeecookbook.view

import com.jakewharton.rxbinding2.InitialValueObservable
import me.khol.ackeecookbook.domain.model.RecipeDetail
import me.khol.ackeecookbook.view.base.BaseView

/**
 * Created by DAVE on 17. 7. 2017.
 *
 * View for RecipeDetail screen
 */
interface RecipeDetailView : BaseView {

    fun setRecipe(recipe: RecipeDetail)

    fun getRatingChanges(): InitialValueObservable<Float>

    fun disableRatingBar()

    fun showDetailsLoading(show: Boolean)

    fun showDetailsNotAvailable()

    fun showRateRecipeLoading(show: Boolean)

}