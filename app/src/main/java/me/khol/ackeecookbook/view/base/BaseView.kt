package me.khol.ackeecookbook.view.base

/**
 * Created by DAVE on 17. 7. 2017.
 *
 * Base view that all views should extend from. These methods should be implemented in
 * [me.khol.ackeecookbook.activity.base.BaseNucleusActivity], if possible.
 */
interface BaseView {

    fun showToast(text: CharSequence)

    fun showToast(resource: Int)

    fun showSnack(text: CharSequence)

    fun showSnack(resource: Int)

}