package me.khol.ackeecookbook.view

import me.khol.ackeecookbook.domain.model.Recipe
import me.khol.ackeecookbook.view.base.BaseView

/**
 * Created by DAVE on 17. 7. 2017.
 *
 * View for RecipeList screen
 */
interface RecipeListView : BaseView {

    fun showRefreshing(refreshing: Boolean)

    fun showRecipesUsingDiff(newRecipes: List<Recipe>)

    fun showRecipes(newRecipes: List<Recipe>, moreAvaiable: Boolean)

    fun addRecipes(newRecipes: List<Recipe>, moreAvaiable: Boolean)

}