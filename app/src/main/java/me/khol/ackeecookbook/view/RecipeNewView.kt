package me.khol.ackeecookbook.view

import io.reactivex.Observable
import io.reactivex.functions.Consumer
import me.khol.ackeecookbook.view.base.BaseView

/**
 * Created by DAVE on 17. 7. 2017.
 *
 * View for RecipeNew screen
 */
interface RecipeNewView : BaseView {

    fun showIngredients(ingredients: MutableList<String>)

    fun showNameError(message: String?)

    fun showIntroductionError(message: String?)

    fun showDurationError(message: String?)

    fun showProcessError(message: String?)


    fun getIngredients(): List<String>


    fun getNewIngredientClicks(): Observable<Any>

    fun getNameChanges(): Observable<CharSequence>

    fun getIntroductionChanges(): Observable<CharSequence>

    fun getProcessChanges(): Observable<CharSequence>

    fun getDurationChanges(): Observable<Int>

    fun getSaveClicks(): Observable<Any>


    fun getSaveMenuItem(): Consumer<in Boolean>


    fun onSaveStarted()

    fun onSaveFinishedSuccess()

    fun onSaveFinishedError(message: String)

}