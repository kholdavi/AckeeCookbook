package me.khol.ackeecookbook.di

import com.hannesdorfmann.sqlbrite.dao.DaoManager
import dagger.Module
import dagger.Provides
import me.khol.ackeecookbook.App
import me.khol.ackeecookbook.Constants
import me.khol.ackeecookbook.dao.RecipeDao
import javax.inject.Singleton



/**
 * Module handling basic dependency injection of App and DAOs
 *
 * Created by DAVE on 18. 7. 2017.
 */
@Module(
    includes = arrayOf(InteractorsModule::class)
)
open class AppModule(val app: App) {

//    val recipeDao: RecipeDao = RecipeDao()
//
//    init {
//        DaoManager.with(app)
//                .databaseName(Constants.DB_NAME)
//                .version(Constants.DB_VERSION)
//                .logging(false)
//                .add(recipeDao)
//                .build()
//    }

    @Provides
    @Singleton
    fun provideApplication(): App {
        return app
    }

}
