package me.khol.ackeecookbook.di

import android.app.Application
import dagger.Module
import dagger.Provides
import me.khol.ackeecookbook.domain.ApiDescription
import me.khol.ackeecookbook.interactor.ApiInteractorImpl
import me.khol.ackeecookbook.interactor.IApiInteractor
import me.khol.ackeecookbook.interactor.SPInteractor
import javax.inject.Singleton

/**
 * Module handling dependency injection of Interactors
 *
 * Created by DAVE on 18. 7. 2017.
 */
@Module(
    includes = arrayOf(ServiceModule::class)
)
class InteractorsModule {

    val TAG = javaClass.name

    @Provides
    @Singleton
    fun provideApiInteractor(apiDescription: ApiDescription): IApiInteractor {
        return ApiInteractorImpl(apiDescription)
    }

    @Provides
    @Singleton
    fun provideSPInteractor(app: Application): SPInteractor {
        return SPInteractor(app)
    }

}
