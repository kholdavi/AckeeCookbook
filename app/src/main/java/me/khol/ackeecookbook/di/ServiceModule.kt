package me.khol.ackeecookbook.di

import android.util.Log
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.*
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Singleton
import dagger.Module
import dagger.Provides
import me.khol.ackeecookbook.ApiConfig
import me.khol.ackeecookbook.App
import me.khol.ackeecookbook.Utils
import me.khol.ackeecookbook.domain.ApiDescription
import me.khol.ackeecookbook.interactor.HttpLoggingInterceptor
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.Interceptor
import org.joda.time.format.ISODateTimeFormat
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * Module handling dependency injection of API related stuff
 *
 * Created by DAVE on 18. 7. 2017.
 */
@Module()
class ServiceModule {

    val TAG = javaClass.name

    @Provides
    @Singleton
    fun provideApiDescription(): ApiDescription {
        return Retrofit.Builder()
                .baseUrl(ApiConfig.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GSON))
                .client(okHttpClient)
                .build()
                .create(ApiDescription::class.java)
    }

    private val okHttpClient: OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(offlineCacheInterceptor)
            .addNetworkInterceptor(cacheInterceptor)
            .addNetworkInterceptor(StethoInterceptor())
            .cache(okHTTPCache)
            .build()

    private val okHTTPCache: Cache
        get() = Cache(File(App.getInstance().cacheDir, "http-cache"), 1024 * 1024 * 10)


    private val loggingInterceptor: HttpLoggingInterceptor
        get() {
            val httpLoggingInterceptor = HttpLoggingInterceptor { message ->
                val maxLogSize = 1000
                for (i in 0..message.length / maxLogSize) {
                    val start = i * maxLogSize
                    var end = (i + 1) * maxLogSize
                    end = if (end > message.length) message.length else end
                    Log.d(TAG, message.substring(start, end))
                }
            }
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS)
            return httpLoggingInterceptor
        }

    private val offlineCacheInterceptor: Interceptor
        get() = Interceptor { chain ->
            val request = chain.request()
            if (!Utils.isConnected()) {
                val cacheControl: CacheControl = CacheControl.Builder()
                        .maxStale(7, TimeUnit.DAYS)
                        .build()
                val newRequest = request.newBuilder()
                        .cacheControl(cacheControl)
                        .build()
                chain.proceed(newRequest)
            } else {
                chain.proceed(request)
            }
        }

    private val cacheInterceptor: Interceptor
        get() = Interceptor { chain ->
            val request = chain.request()
            val response = chain.proceed(request)
            val cacheControl: CacheControl = CacheControl.Builder()
                    .maxAge(2, TimeUnit.MINUTES)
                    .build()
            response.newBuilder()
                    .header("Cache-Control", cacheControl.toString())
                    .build()
        }


    companion object {

        val serial = JsonSerializer<Date> { src, _, _->
            if (src == null) null else JsonPrimitive(src.time)
        }

        val deserial = JsonDeserializer<Date> { json, _, _ ->
            val parser = ISODateTimeFormat.dateTimeParser()
            if (json == null) null else parser.parseDateTime(json.asString).toDate()
        }

        val GSON = GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .registerTypeAdapter(Date::class.java, serial)
                .registerTypeAdapter(Date::class.java, deserial)
                .create()
    }


}