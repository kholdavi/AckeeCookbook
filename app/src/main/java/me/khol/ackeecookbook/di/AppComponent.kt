package me.khol.ackeecookbook.di

import dagger.Component
import me.khol.ackeecookbook.App
import me.khol.ackeecookbook.presenter.RecipeDetailPresenter
import me.khol.ackeecookbook.presenter.RecipeListPresenter
import me.khol.ackeecookbook.presenter.RecipeNewPresenter
import javax.inject.Singleton

/**
 * Main application component
 *
 * Created by DAVE on 18. 7. 2017.
 */
@Singleton
@Component(
    modules = arrayOf(AppModule::class)
)
interface AppComponent {

    fun inject(presenter: RecipeListPresenter)

    fun inject(presenter: RecipeDetailPresenter)

    fun inject(presenter: RecipeNewPresenter)

}