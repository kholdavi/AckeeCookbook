package me.khol.ackeecookbook

/**
 * Constants
 *
 * Created by DAVE on 18. 7. 2017.
 */
object Constants {
    val DB_NAME: String = "Cookbook"
    val DB_VERSION: Int = 1

}
