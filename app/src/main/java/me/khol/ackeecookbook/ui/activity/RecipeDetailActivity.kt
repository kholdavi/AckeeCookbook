package me.khol.ackeecookbook.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.jakewharton.rxbinding2.InitialValueObservable
import com.jakewharton.rxbinding2.widget.RxRatingBar
import kotlinx.android.synthetic.main.activity_recipe_detail.*

import me.khol.ackeecookbook.R
import me.khol.ackeecookbook.domain.model.Recipe
import me.khol.ackeecookbook.domain.model.RecipeDetail
import me.khol.ackeecookbook.ui.activity.base.BaseNucleusActivity
import me.khol.ackeecookbook.presenter.RecipeDetailPresenter
import me.khol.ackeecookbook.view.RecipeDetailView
import nucleus5.factory.RequiresPresenter

/**
 * Created by DAVE on 17. 7. 2017.
 *
 * Activity for RecipeDetail screen
 */
@RequiresPresenter(RecipeDetailPresenter::class)
class RecipeDetailActivity : BaseNucleusActivity<RecipeDetailPresenter>(), RecipeDetailView {

    companion object {
        val ARG_ARGS = "ARGS"
        val ARG_RECIPE = "RECIPE"
        val ARG_RECIPE_ID = "RECIPE_ID"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe_detail)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_white)


        val bundleExtra = intent.getBundleExtra(ARG_ARGS)
        val recipe: Recipe = bundleExtra.getParcelable(RecipeDetailActivity.ARG_RECIPE)

        toolbar_layout.title = recipe.name
        ratingBar_rating.rating = recipe.score
        txt_time.text = getString(R.string.time_duration, recipe.duration)

        presenter.init(bundleExtra)
    }

    override fun getRatingChanges(): InitialValueObservable<Float> {
        return RxRatingBar.ratingChanges(ratingBar_rate_recipe)
    }

    override fun setRecipe(recipe: RecipeDetail) {
        toolbar_layout.title = recipe.name
        ratingBar_rating.rating = (recipe.score ?: 0f)
        txt_time.text = getString(R.string.time_duration, recipe.duration)
        txt_description.text = recipe.description
        txt_info.text = recipe.info
        txt_ingredients.text = recipe.ingredients
                .fold(StringBuilder()) { acc: StringBuilder, ingredient: String ->
                    acc.append("•\t\t$ingredient\n")
                }.toString()

    }

    override fun showDetailsLoading(show: Boolean) {
        if (show) {
            progress_details.visibility = View.VISIBLE
            layout_details.visibility = View.GONE
        } else {
            progress_details.visibility = View.GONE
            layout_details.visibility = View.VISIBLE
        }
    }

    override fun showDetailsNotAvailable() {
        progress_details.visibility = View.GONE
        layout_details.visibility = View.GONE
    }

    override fun showRateRecipeLoading(show: Boolean) {
        if (show) {
            progress_rate_recipe.visibility = View.VISIBLE
            layout_rate_recipe.visibility = View.GONE
        } else {
            progress_rate_recipe.visibility = View.GONE
            layout_rate_recipe.visibility = View.VISIBLE
        }
    }

    override fun disableRatingBar() {
        ratingBar_rate_recipe.isEnabled = false
        txt_rate_recipe.setText(R.string.recipe_detail_rate_this_recipe_thanks)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_recipe_detail, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_add -> {
                startActivity(Intent(this, RecipeNewActivity::class.java))
                return true
            } else -> {
                return super.onOptionsItemSelected(item)
            }
        }
    }

}
