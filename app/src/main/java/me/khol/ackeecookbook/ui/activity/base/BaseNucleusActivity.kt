package me.khol.ackeecookbook.ui.activity.base

import android.content.Context
import android.support.design.widget.Snackbar
import android.view.MenuItem
import android.widget.Toast
import me.khol.ackeecookbook.view.base.BaseView
import nucleus5.presenter.Presenter
import nucleus5.view.NucleusAppCompatActivity
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

/**
 * Created by DAVE on 17. 7. 2017.
 *
 * Base NucleusActivity class that all Activities using Nucleus should extend from
 */
open class BaseNucleusActivity<P: Presenter<*>> : NucleusAppCompatActivity<P>(), BaseView {


    override fun showToast(text: CharSequence) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }

    override fun showToast(resource: Int) {
        Toast.makeText(this, resource, Toast.LENGTH_LONG).show()
    }

    override fun showSnack(text: CharSequence) {
        val contentView = findViewById(android.R.id.content)
        Snackbar.make(contentView, text, Snackbar.LENGTH_LONG).show()
    }

    override fun showSnack(resource: Int) {
        val contentView = findViewById(android.R.id.content)
        Snackbar.make(contentView, resource, Snackbar.LENGTH_LONG).show()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            } else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

}