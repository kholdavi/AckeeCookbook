package me.khol.ackeecookbook.ui.activity

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.Window
import com.jakewharton.rxbinding2.InitialValueObservable
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.Observable

import me.khol.ackeecookbook.R
import me.khol.ackeecookbook.ui.activity.base.BaseNucleusActivity
import me.khol.ackeecookbook.presenter.RecipeNewPresenter
import me.khol.ackeecookbook.view.RecipeNewView
import kotlinx.android.synthetic.main.activity_recipe_new.*
import me.khol.ackeecookbook.ui.adapter.IngredientsAdapter
import nucleus5.factory.RequiresPresenter
import com.jakewharton.rxbinding2.view.RxMenuItem
import io.reactivex.functions.Consumer
import me.khol.ackeecookbook.BuildConfig
import me.khol.ackeecookbook.Utils
import me.khol.ackeecookbook.ui.dialog.TimePickerDialogFragment
import android.content.Context.LAYOUT_INFLATER_SERVICE
import android.view.LayoutInflater




/**
 * Created by DAVE on 17. 7. 2017.
 *
 * Activity for RecipeNew screen
 */
@RequiresPresenter(RecipeNewPresenter::class)
open class RecipeNewActivity : BaseNucleusActivity<RecipeNewPresenter>(), RecipeNewView, TimePickerDialogFragment.Callback {

    val TAG = javaClass.name

    private lateinit var adapter: IngredientsAdapter
    private lateinit var saveMenuItem: MenuItem


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_recipe_new)

        toolbar.setTitle(R.string.title_activity_recipe_new)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_primary)

        adapter = IngredientsAdapter(mutableListOf())

        val manager = LinearLayoutManager(this)
        recycler_ingredients.layoutManager = manager
        recycler_ingredients.itemAnimator = DefaultItemAnimator()
        recycler_ingredients.adapter = adapter


        txt_duration.isFocusable = false
        txt_duration.setOnClickListener {
            val duration = if (txt_duration.text.isBlank()) {
                30
            } else {
                Utils.stripTime(txt_duration.text)
            }

            val newFragment = TimePickerDialogFragment.newInstance(duration)
            newFragment.show(supportFragmentManager, "dialog")
        }


        if (BuildConfig.DEBUG) {
            txt_name.setTextKeepState("Donut Ackee")
            txt_introduction.setTextKeepState("Very delicious dessert that will satisfy everyone's needs")
            txt_process.setTextKeepState("Easy to make! Just add baking powder and water.")
            timePickerUpdated(10)
        }
    }


    override fun showIngredients(ingredients: MutableList<String>) {
        adapter.showIngredients(ingredients)
    }

    override fun timePickerUpdated(time: Int) {
        txt_duration.setText(getString(R.string.time_duration, time))
    }

    override fun getNewIngredientClicks(): Observable<Any> {
        return RxView.clicks(btn_new_ingredient)
    }


    override fun showNameError(message: String?) {
        inputLayout_name.error = message
    }

    override fun showIntroductionError(message: String?) {
        inputLayout_introduction.error = message
    }

    override fun showDurationError(message: String?) {
        inputLayout_duration.error = message
    }

    override fun showProcessError(message: String?) {
        inputLayout_process.error = message
    }


    override fun getIngredients(): List<String> {
        return adapter.getIngredients()
    }


    override fun getNameChanges(): Observable<CharSequence> {
        return RxTextView.textChanges(txt_name)
    }

    override fun getIntroductionChanges(): Observable<CharSequence> {
        return RxTextView.textChanges(txt_introduction)
    }

    override fun getProcessChanges(): Observable<CharSequence> {
        return RxTextView.textChanges(txt_process)
    }

    override fun getDurationChanges(): Observable<Int> {
        return RxTextView.textChanges(txt_duration)
                .map {
                    if (it.isBlank()) {
                        0
                    } else {
                        Utils.stripTime(it)
                    }
                }
    }


    override fun getSaveClicks(): Observable<Any> {
        return RxMenuItem.clicks(saveMenuItem)
    }

    override fun getSaveMenuItem(): Consumer<in Boolean> {
        return RxMenuItem.enabled(saveMenuItem)
    }


    override fun onSaveStarted() {
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.progress_layout, null)
        saveMenuItem.actionView = view
    }

    override fun onSaveFinishedSuccess() {
        saveMenuItem.actionView = null
        showToast(R.string.recipe_new_save_success)
        finish()
    }

    override fun onSaveFinishedError(message: String) {
        saveMenuItem.actionView = null
        showSnack(message)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_recipe_new, menu)
        saveMenuItem = menu.findItem(R.id.action_add)

        // TODO: fix in some other way
        // Resorted to deferring of presenter initialization because saveMenuItem is not
        // initialized at the time of Presenter.onTakeView(view)
        // Maybe breaks when rotate screen -> onTakeView doesn't get put through
        presenter.init(this)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        when (item.itemId) {
//            R.id.action_add -> {
//                presenter.saveRecipe()
//                return true
//        }
        return super.onOptionsItemSelected(item)
    }

}
