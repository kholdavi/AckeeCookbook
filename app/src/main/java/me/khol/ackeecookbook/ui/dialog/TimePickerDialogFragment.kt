package me.khol.ackeecookbook.ui.dialog

import android.app.Dialog
import android.content.Context
import android.support.v4.app.DialogFragment
import android.os.Bundle
import me.khol.ackeecookbook.R
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.widget.NumberPicker


/**
 * Time picker dialog with values ranging from 5 min. to 240 min.
 *
 * Created by DAVE on 19. 7. 2017.
 */
class TimePickerDialogFragment : DialogFragment() {

    companion object {

        private var ARG_TIME = "time"

        fun newInstance(time: Int): TimePickerDialogFragment {
            val arguments = Bundle()
            arguments.putInt(ARG_TIME, time)

            val fragment = TimePickerDialogFragment()
            fragment.arguments = arguments
            return fragment
        }
    }

    private fun values(min: Int, max: Int, step: Int): Array<String?> {
        val values = arrayOfNulls<String>(max / min)
        var i = min
        while (i <= max) {
            values[i / step - 1] = context.getString(R.string.time_duration, i)
            i += step
        }
        return values
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view =  inflater.inflate(R.layout.time_picker_dialog, null, false)
        val numberPicker = view.findViewById<NumberPicker>(R.id.numberPicker)

        val minValue = 5
        val maxValue = 240
        val step = 5

        numberPicker.minValue = minValue / step
        numberPicker.maxValue = maxValue / minValue
        numberPicker.displayedValues = values(minValue, maxValue, step)
        // set possible values first and set correct value afterwards
        numberPicker.value = arguments.getInt(ARG_TIME) / step

        return AlertDialog.Builder(activity)
                .setTitle(R.string.recipe_new_time)
                .setView(view)
                .setPositiveButton(R.string.alert_dialog_ok,
                        { _: DialogInterface, _: Int ->
                            (activity as Callback).timePickerUpdated(numberPicker.value * step)
                        }
                )
                .setNegativeButton(R.string.alert_dialog_cancel,
                        { _, _ ->
                            // just dismiss
                        }
                )
                .create()
    }


    interface Callback {

        fun timePickerUpdated(time: Int)

    }

}