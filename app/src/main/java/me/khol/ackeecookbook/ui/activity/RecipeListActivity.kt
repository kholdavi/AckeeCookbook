package me.khol.ackeecookbook.ui.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import me.khol.ackeecookbook.R
import me.khol.ackeecookbook.ui.activity.base.BaseNucleusActivity
import me.khol.ackeecookbook.ui.adapter.RecipesAdapter
import me.khol.ackeecookbook.domain.model.Recipe
import me.khol.ackeecookbook.presenter.RecipeListPresenter
import me.khol.ackeecookbook.view.RecipeListView
import kotlinx.android.synthetic.main.activity_recipe_list.*
import android.support.v7.widget.DividerItemDecoration
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator
import nucleus5.factory.RequiresPresenter
import android.support.v7.widget.RecyclerView
import android.view.animation.DecelerateInterpolator


/**
 * Created by DAVE on 17. 7. 2017.
 *
 * Activity for RecipeList screen
 */
@RequiresPresenter(RecipeListPresenter::class)
open class RecipeListActivity : BaseNucleusActivity<RecipeListPresenter>(), RecipeListView {

    val TAG = javaClass.name

    private lateinit var adapter: RecipesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe_list)

        toolbar.setTitle(R.string.title_activity_recipe_list)
        setSupportActionBar(toolbar)

        refresh_layout.setColorSchemeResources(R.color.colorAccent)
        refresh_layout.setOnRefreshListener { presenter.refreshRecipes() }

        adapter = RecipesAdapter(listOf()) { recipe: Recipe ->
            val intent = Intent(this, RecipeDetailActivity::class.java)
            val args = Bundle()
            args.putParcelable(RecipeDetailActivity.ARG_RECIPE, recipe)
            args.putString(RecipeDetailActivity.ARG_RECIPE_ID, recipe.id)
            intent.putExtra(RecipeDetailActivity.ARG_ARGS, args)
            startActivity(intent)
        }

        val manager = object : LinearLayoutManager(this) {
            /*  According to
                    https://stackoverflow.com/questions/31759171/recyclerview-and-java-lang-indexoutofboundsexception-inconsistency-detected-in
                recyclerView has a bug that may cause IndexOutOfBoundsException.
                Catching the exception fixes the problem and shouldn't introduce another problems
             */
            override fun onLayoutChildren(recycler: RecyclerView.Recycler?, state: RecyclerView.State) {
                try {
                    super.onLayoutChildren(recycler, state)
                } catch (ignore: IndexOutOfBoundsException) {
                }
            }
        }

        val recyclerViewOnScrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleCount = manager.childCount
                val totalCount = manager.itemCount
                val firstVisiblePosition = manager.findFirstVisibleItemPosition()

                if (visibleCount + firstVisiblePosition >= totalCount &&
                    firstVisiblePosition >= 0) {
                    presenter.downloadMoreRecipes()
                }
            }
        }

        recycler_list.layoutManager = manager
        recycler_list.adapter = adapter
        recycler_list.itemAnimator = SlideInUpAnimator(DecelerateInterpolator(1f))
        recycler_list.addItemDecoration(DividerItemDecoration(this, manager.orientation))
        recycler_list.addOnScrollListener(recyclerViewOnScrollListener)
    }


    override fun showRefreshing(refreshing: Boolean) {
        refresh_layout.isRefreshing = refreshing
    }

    @Deprecated("Interesting concept, but keeps shifting items rather randomly")
    override fun showRecipesUsingDiff(newRecipes: List<Recipe>) {
        adapter.showRecipesUsingDiff(newRecipes)
    }

    override fun showRecipes(newRecipes: List<Recipe>, moreAvaiable: Boolean) {
        recycler_list.recycledViewPool.clear()
        adapter.showRecipes(newRecipes, moreAvaiable)
    }

    override fun addRecipes(newRecipes: List<Recipe>, moreAvaiable: Boolean) {
        recycler_list.recycledViewPool.clear()
        adapter.addRecipes(newRecipes, moreAvaiable)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_recipe_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_add -> {
                startActivity(Intent(this, RecipeNewActivity::class.java))
                return true
            } else -> {
                return super.onOptionsItemSelected(item)
            }
        }
    }

}
