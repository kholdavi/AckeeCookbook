package me.khol.ackeecookbook.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_recipe_list.view.*
import me.khol.ackeecookbook.R
import me.khol.ackeecookbook.domain.model.Recipe
import android.support.v7.util.DiffUtil



/**
 * Created by DAVE on 17. 7. 2017.
 *
 * Adapter that handles all types of recipes displayed in a list on a main page of the app
 */
open class RecipesAdapter(private var recipes: List<Recipe>,
                          private val listener: (Recipe) -> Unit = {}) : RecyclerView.Adapter<RecipesAdapter.ViewHolder>() {

    private val TYPE_BASIC = 0
    private val TYPE_LOADING = 1

    var moreAvailable: Boolean = true


    @Deprecated("Interesting concept, but keeps shifting items rather randomly")
    fun showRecipesUsingDiff(newRecipes: List<Recipe>) {
        val oldRecipes: List<Recipe> = recipes.toList()
        val diff = RecipesDiffCallback(oldRecipes, newRecipes)
        val diffResult = DiffUtil.calculateDiff(diff)

        recipes = newRecipes

        diffResult.dispatchUpdatesTo(this)
    }

    fun showRecipes(newRecipes: List<Recipe>, moreAvailable: Boolean) {
        this.moreAvailable = moreAvailable
        recipes = newRecipes
        notifyDataSetChanged()
    }

    fun addRecipes(newRecipes: List<Recipe>, moreAvailable: Boolean) {
        this.moreAvailable = moreAvailable
        val mutableRecipes = this.recipes.toMutableList()
        mutableRecipes.addAll(newRecipes)
        recipes = mutableRecipes
        notifyItemRangeInserted(recipes.size - newRecipes.size, newRecipes.size)
    }

    override fun getItemViewType(position: Int): Int {
        if (position == itemCount - 1 && moreAvailable)
            return TYPE_LOADING
        else
            return TYPE_BASIC
    }

    override fun getItemCount(): Int {
        return if (moreAvailable)
            recipes.size + 1
        else
            recipes.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (holder.itemViewType) {
            TYPE_BASIC -> {
                val item = recipes[position]

                holder.bindItem(item)
                holder.view.setOnClickListener {
                    listener(item)
                }
            }
            TYPE_LOADING -> {
                // don't need to do anything
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return when (viewType) {
            TYPE_BASIC -> {
                ViewHolder.Basic(inflater.inflate(R.layout.item_recipe_list, parent, false))
            }
            TYPE_LOADING -> {
                ViewHolder.Loading(inflater.inflate(R.layout.item_recipe_list_loading, parent, false))
            }
            else -> {
                throw IllegalArgumentException("Illegal type")
            }
        }
    }



    sealed class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        abstract fun bindItem(item: Recipe)


        class Basic(view: View) : ViewHolder(view) {

            lateinit var recipe: Recipe

            override fun bindItem(item: Recipe) {
                recipe = item

                view.txt_title.text = recipe.name
                view.ratingBar_rating.rating = recipe.score
                view.txt_time.text = view.context.getString(R.string.time_duration, recipe.duration)
            }
        }

        class Loading(view: View) : ViewHolder(view) {

            override fun bindItem(item: Recipe) {

            }
        }
    }


    class RecipesDiffCallback(internal var newRecipes: List<Recipe>,
                              internal var oldRecipes: List<Recipe>) : DiffUtil.Callback() {

        override fun getOldListSize(): Int {
            return oldRecipes.size
        }

        override fun getNewListSize(): Int {
            return newRecipes.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldRecipes[oldItemPosition].id === newRecipes[newItemPosition].id
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldRecipes[oldItemPosition] == newRecipes[newItemPosition]
        }

        override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
            //you can return particular field for changed item.
            return super.getChangePayload(oldItemPosition, newItemPosition)
        }
    }


}


