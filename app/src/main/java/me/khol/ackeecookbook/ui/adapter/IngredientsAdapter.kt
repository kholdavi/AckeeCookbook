package me.khol.ackeecookbook.ui.adapter

import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_recipe_new_ingredient.view.*
import me.khol.ackeecookbook.R

/**
 * Created by DAVE on 17. 7. 2017.
 *
 * Adapter that ingredients
 */
open class IngredientsAdapter(private var ingredients: MutableList<String>) : RecyclerView.Adapter<IngredientsAdapter.ViewHolder>() {


    fun showIngredients(ingredients: MutableList<String>) {
        this.ingredients = ingredients
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_recipe_new_ingredient, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return ingredients.size
    }

    fun getIngredients(): List<String> {
        return ingredients.toList()
    }


    inner open class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        var pos: Int = -1

        init {
            view.txt_ingredient.addTextChangedListener(object: TextWatcher {
                override fun afterTextChanged(p0: Editable) {
                    ingredients[pos] = p0.toString()
                }
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }
            })
            view.txt_ingredient.onFocusChangeListener = View.OnFocusChangeListener {
                view, hasFocus ->
                if (!hasFocus && view.txt_ingredient.text.isEmpty()) {
                    ingredients.removeAt(pos)
                    notifyDataSetChanged()
                }
            }
        }

        fun bindItem(position: Int) {
            this.pos = position

            view.txt_ingredient.setTextKeepState(ingredients[pos])
        }

    }

}


