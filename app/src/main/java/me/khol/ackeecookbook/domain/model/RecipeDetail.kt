package me.khol.ackeecookbook.domain.model

import android.annotation.SuppressLint
import io.mironov.smuggler.AutoParcelable

/**
 * Created by DAVE on 17. 7. 2017.
 *
 * Data class describing detailed information about one instance of Recipe
 */
@SuppressLint("ParcelCreator")
data class RecipeDetail(
        val name: String,
        val description: String,
        val duration: Int,
        val ingredients: List<String>,
        val info: String,
        val id: String?,
        val score: Float?
): AutoParcelable
