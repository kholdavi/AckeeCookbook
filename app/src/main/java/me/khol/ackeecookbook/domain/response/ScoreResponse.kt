package me.khol.ackeecookbook.domain.response

/**
 * Score Response
 *
 * Created by DAVE on 19. 7. 2017.
 */
class ScoreResponse(val score: Float, val recipe: String, val id: String)
