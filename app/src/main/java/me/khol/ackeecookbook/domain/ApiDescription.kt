package me.khol.ackeecookbook.domain

import io.reactivex.Completable
import io.reactivex.Single
import me.khol.ackeecookbook.domain.model.Recipe
import me.khol.ackeecookbook.domain.model.RecipeDetail
import me.khol.ackeecookbook.domain.model.Score
import me.khol.ackeecookbook.domain.model.UpdateBody
import me.khol.ackeecookbook.domain.response.ScoreResponse
import retrofit2.http.*

/**
 * REST API description
 *
 * Created by DAVE on 18. 7. 2017.
 */
interface ApiDescription {

    @GET("api/v1/recipes")
    fun getRecipes(): Single<List<Recipe>>

    @POST("api/v1/recipes")
    fun createRecipe(@Body recipe: RecipeDetail): Single<RecipeDetail>

    @GET("api/v1/recipes/{recipeId}")
    fun getRecipe(@Path("recipeId") recipeId: String): Single<RecipeDetail>

    // unused, untested
    @PUT("api/v1/recipes/{recipeId}")
    fun updateRecipe(@Path("recipeId") recipeId: String, @Body body: RecipeDetail): Completable

    // unused, untested
    @DELETE("api/v1/recipes/{recipeId}")
    fun deleteRecipe(@Path("recipeId") recipeId: String): Completable

    @POST("api/v1/recipes/{recipeId}/ratings")
    fun addRating(@Path("recipeId") recipeId: String, @Body body: Score): Single<ScoreResponse>


}