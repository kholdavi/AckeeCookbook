package me.khol.ackeecookbook.domain.model

import android.annotation.SuppressLint
import io.mironov.smuggler.AutoParcelable

/**
 * Created by DAVE on 17. 7. 2017.
 *
 * Data class describing information about one instance of Recipe
 */
@SuppressLint("ParcelCreator")
data class Recipe(
        val id: String,
        val name: String,
        val score: Float,
        val duration: Int
): AutoParcelable
