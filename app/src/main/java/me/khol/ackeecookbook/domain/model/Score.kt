package me.khol.ackeecookbook.domain.model

/**
 * Used for updating score of a recipe
 *
 * Created by DAVE on 18. 7. 2017.
 */
data class Score(val score: Int)