package me.khol.ackeecookbook.domain.response

/**
 * Error response
 *
 * Created by DAVE on 19. 7. 2017.
 */
data class ErrorResponse(val err: ErrorDetails, val message: String)

data class ErrorDetails(val name: String, val status: Int, val errorCode: Int)

//{
//    "err": {
//        "name":"ValidationError",
//        "status":422,
//        "errorCode":0
//    },
//    "message":"Recipe with this name already exists"
//}

//{
//    "err": {
//        "name": "BadRequestError",
//        "status": 400,
//        "errorCode": 0
//    },
//    "message": "Score must be between 0 and 5"
//}
